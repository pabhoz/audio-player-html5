
$(function(){

	var audio = $("audio")[0];
	var elapsedTime = $(".data .controls .timer");
	var playerGUI = $(".player")[0];
	var listaReproduccion = document.getElementById("playList");	

	audio.volume = 0.7;

	var botones = {
		play: {
			domElement: $(".buttons .botones .button.play"),
			click: function(){
				this.playToggle(audio);
			},
			playToggle: function(player){
				if(player.paused){
					player.play();
				}else{
					player.pause();
				}
				this.domElement.toggleClass("pause");
			}
		},
		stop: {},
		next: {},
		back: {},
		fback: {},
		fnext: {}
	}

	/* Listeners */
	botones.play.domElement.click(function(){
		botones.play.click();
	});

	audio.addEventListener("timeupdate",function(){
		var totalSec = this.currentTime;
		var hours = parseInt(totalSec/3600) % 24;
		var minutes = parseInt(totalSec/60) % 60;
		var seconds = parseInt(totalSec % 60, 10);

		var result = (hours < 10 ? "0" + hours : hours) +":"+ (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
		elapsedTime.html(result);

		playerGUI.querySelector(".controls .time .elapsed").style.width = (( 100 / this.duration ) * this.currentTime) + "%";

	}, false);

	audio.addEventListener("volumechange",function(){
		playerGUI.querySelector(".controls .volume .ammount").style.width = (this.volume * 100 ) + "%";
	});

	playerGUI.querySelector(".controls .time").addEventListener("click",function(e){
			console.log(e);
			var clickPos = e.offsetX;
			var songSize = audio.duration;
			var barSize = e.target.offsetWidth;
			audio.currentTime = (clickPos/barSize) * songSize;
	});

	playerGUI.querySelector(".controls .volume .volSlider").addEventListener("click",function(e){
			var clickPos = e.offsetX;
			var volumeSize = audio.volume;
			var barSize = e.target.offsetWidth;
			audio.volume = clickPos/barSize;
	});
	
	$("#folderChooser").change(function(e){
		var files = e.target.files;
		var songs = new Array();
		for (var i = 0; i < files.length; i++) {
			if(files[i].type == "audio/mp3"){

				getID3(files[i],function(args){
					console.debug(args);
					listaReproduccion.innerHTML += '<div class="enCola" data-album="'+args[0].album+'" data-year="'+args[0].year+'" data-src="'+args[1].webkitRelativePath+'" data-genre="'+args[0].genre+'" data-track="'+args[0].track+'" onclick="changeSong(this)"><div class="title">'+args[0].title+'</div><div class="artist">'+args[0].artist+'</div> - <div class="album">'+args[0].album+'</div></div>';
				});
			}
		};
	});

});

function changeSong(song){
	console.log(song);
	$("audio")[0].src = song.dataset.src;
}

function getID3(file,callback){

	ID3.loadTags(file,function(){
			var tags = ID3.getAllTags(file);

			console.log(file);

			if("picture" in tags){
				loadCover(tags);
			}

			if(callback){
				callback([tags,file]);
			}
			return [tags,file];

		}, {
			tags: ["artist", "title", "album", "year", "comment", "track", "genre", "lyrics", "picture"],
			dataReader: FileAPIReader(file)
		});

}









