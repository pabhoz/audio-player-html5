$(function(){

	listaReproduccion = document.getElementById("playList");
	var dropArea = document.querySelector('#dropArea canvas');

	var filesURIs = new Array(),
	reminder = 1,
	lastID = 0,
	totalFiles = 0,
	toLoad = 0,
	alreadyLoaded = 0;

	dropArea.ondragover = function(evt){
		evt.preventDefault();
	}

	dropArea.ondrop = function(evt){
		evt.preventDefault();

		var files = evt.dataTransfer.files;
		var reader = new FileReader();

		toLoad = files.length;
		totalFiles = files.length;

		if(files[0].type.match("audio")){

				console.debug("audio incomming");
				loadSong(files[0],reader);
				reminder = 1;

		}else{
			console.debug("invader detected from planet: "+files[0].type);
		}

		reader.onerror = function(evt){

		}

		reader.onload = function(evt){
			console.debug("carga de archivo");

			var contenido = reader.result;
			filesURIs.push(contenido);

			toLoad--;
			alreadyLoaded++;
			console.log("to load: "+toLoad+" loaded: "+alreadyLoaded);

			if(toLoad>0){
				asignSrc(alreadyLoaded-1);
				loadSong(files[alreadyLoaded],reader);

				var progress = Math.round(((alreadyLoaded)*100) / totalFiles,1);
				//updateLoadingBar(progress);
			}

			if(alreadyLoaded == totalFiles){
				//updateLoadingBar(100);
				toLoad = 0;
				alreadyLoaded = 0;
				totalFiles = 0;
			}

		}

		reader.onprogress = function(evt){
			console.log("Progress: "+evt.loaded + " from "+evt.total);
		}
	}
	
	function asignSrc(index){
		$('.enCola')[index].dataset.src = filesURIs[index];
	}
	
	function loadSong(file,reader){
	getID3(file,function(tags){

		listaReproduccion.innerHTML += '<div class="enCola" id="'+lastID+'" data-album="'+tags.album+'" data-year="'+tags.year+'" data-genre="'+tags.genre+'" data-track="'+tags.track+'" onclick="changeSong(this)"><div class="title">'+tags.title+'</div><div class="artist">'+tags.artist+'</div> - <div class="album">'+tags.album+'</div></div>';
		lastID++;
		console.log(tags);
		reader.readAsDataURL(file);
	});
}

function getID3(file,callback){

	ID3.loadTags(file,function(){
			var tags = ID3.getAllTags(file);

					console.log(file);
			
			console.log("carga de ID3");

			if("picture" in tags){
				loadCover(tags);
			}

			if(callback){
				callback(tags);
			}
			return tags;

		}, {
			tags: ["artist", "title", "album", "year", "comment", "track", "genre", "lyrics", "picture"],
			dataReader: FileAPIReader(file)
		});

}

function loadCover(tags){

	jarvisDebug("imagen en los ID3 Tags");
	var image = tags.picture;
	var base64String = "";
	for (var i = 0; i < image.data.length; i++) {
		base64String += String.fromCharCode(image.data[i]);
	};
	$("#mediaCover img")[0].src="data:" + image.format + ";base64,"+ window.btoa(base64String);
}

});